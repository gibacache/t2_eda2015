// Include Guards to avoid double declarations
#ifndef SKIPTREE_H
#define SKIPTREE_H

#include <stdio.h>
#include <stdlib.h>
#include "list.h"

// Estructura del nodo
typedef struct Node
{
  // Altura del nodo
  int height;
  // Lista de llaves en el nodo
  list *key;
  // Lista de hijos (1 más que el numero de llaves)
  list *child;
} Node;

// Estructura del árbol
typedef struct SkipTree
{
  Node *nodes;
} SkipTree;

void skiptree_init();
#endif
