// 11637110 Guillermo Ibacache
// 11635495 Catalina Rosende
#include <stdio.h>
#include <string.h>
#include "qdbmp.h"
#include "skiptree.h"
#include "huffman.h"

// Calculo del tamaño de la imagen
#define IMAGE_SIZE ( width * height * 3 )
// Calculo del tamaño de las dimensiones ancho y alto
#define DIMENSIONS_SIZE ( sizeof( int ) * 2 )

// Imagen bmp
BMP* bmp;
// Variables para guardar el rojo, verde, y azul
UCHAR r, g, b;
// ancho y alto
UINT  width, height;
// Coordenadas del pixel en la imagen
UINT  x, y;
// Archivo
FILE *fp;
// Buffer de entrada y salida para el algoritmo de huffman
UCHAR *in, *out;

/* Compresión de la imagen bmp */
int comprimir(char *imagen, char *comprimido) {

	// Abrir el archivo bmp
	bmp = BMP_ReadFile( imagen );
	// Revisar si hubieron errores al trtar de abrir el archivo
	BMP_CHECK_ERROR( stderr, -1 );
	// Obtener el ancho y alto de la imagen
	width = BMP_GetWidth( bmp );
	height = BMP_GetHeight( bmp );

	// Asignar bloque de memoria para el buffer de entrada
	in = malloc( IMAGE_SIZE + DIMENSIONS_SIZE);
	// Bytes usados en el buffer de entrada
	UINT insize = 0;

	// Codificar el ancho en 4 bytes
	in[insize++] = (width >> 24) & 0xFF;
	in[insize++] = (width >> 16) & 0xFF;
	in[insize++] = (width >> 8) & 0xFF;
	in[insize++] = width & 0xFF;

	// Codificar el alto en 4 bytes
	in[insize++] = (height >> 24) & 0xFF;
	in[insize++] = (height >> 16) & 0xFF;
	in[insize++] = (height >> 8) & 0xFF;
	in[insize++] = height & 0xFF;

	// Ciclo que recorre toda la imagen y codifica cada componente RGB
	for ( y = 0 ; y < height ; ++y )
	{
		for ( x = 0 ; x < width ; ++x )
		{
			BMP_GetPixelRGB( bmp, x, y, &r, &g, &b );
			in[insize++] = r;
			in[insize++] = g;
			in[insize++] = b;
		}
	}

	// Asignar bloque de memoria para el buffer de salida (384 para el header)
	out = calloc( IMAGE_SIZE + DIMENSIONS_SIZE + 384, 1 );

	// Compresión de la imagen y guardar el tamaño del archivo de salida
	int outsize = Huffman_Compress(in, out, insize);

	// Guardar el archivo comprimido
	fp = fopen(comprimido, "wb");
	fwrite(out, 1, outsize, fp);
	fclose(fp);

	// Liberar la memoria asignada
	BMP_Free(bmp);
	free(in);
	free(out);

	return 0;
}

/* Descompresión de la imagen bmp */
int descomprimir(char *imagen, char *comprimido) {

	// Abrir archivo comprimido
	fp = fopen(comprimido, "rb");
	// Obtener el tamaño del archivo
	fseek(fp, 0, SEEK_END);
	long fsize = ftell(fp);
	fseek(fp, 0, SEEK_SET);

	// Asignar bloque de memoria para el buffer de entrada
	in = calloc( fsize + 10, 1 );
	fread(in, fsize, 1, fp);
	fclose(fp);

	// Asignar memoria para leer las minesiones de la imagen
	UCHAR *dimensions = malloc( DIMENSIONS_SIZE );
	// Descomprimir el ancho y el alto
	Huffman_Uncompress(in, dimensions, fsize, DIMENSIONS_SIZE);

	// Inicializar el contador de bytes y las dimensiones
	int current_byte = 0;
	width = height = 0;

	// Decodificar el ancho
	width += dimensions[current_byte++] << 24;
	width += dimensions[current_byte++] << 16;
	width += dimensions[current_byte++] << 8;
	width += dimensions[current_byte++];

	// Decodificar el alto
	height += dimensions[current_byte++] << 24;
	height += dimensions[current_byte++] << 16;
	height += dimensions[current_byte++] << 8;
	height += dimensions[current_byte++];

	// Crear el bmp segun las dimensiones
	bmp = BMP_Create(width, height, 24);

	// Asignar bloque de memoria para el buffer de salida
	out = calloc( IMAGE_SIZE, 1 );
	// Descomprimir la imagen
	Huffman_Uncompress(in, out, fsize, IMAGE_SIZE + DIMENSIONS_SIZE);

	// Recorrer la imagen bmp y guardar los pixeles segun los datos descomprimidos
	for ( y = 0 ; y < height ; ++y )
	{
		for ( x = 0 ; x < width ; ++x )
		{
			r = out[current_byte++];
			g = out[current_byte++];
			b = out[current_byte++];
			BMP_SetPixelRGB( bmp, x, y, r , g , b );
		}
	}

	// Guardar el archivo bmp
	BMP_WriteFile(bmp, imagen);

	// Liberar la memoria asignada
	BMP_Free(bmp);
	free(dimensions);
	free(in);
	free(out);

	return 0;
}

int
main(int argc, char **argv)
{
	// Revisar si el input tiene el formato definido
	if (argc != 4) {
		printf("Uso: %s modo imagen.bmp comprimido\n\nmodo: 0 => comprimir, 1 => descomprimir\n", argv[0] );
		return 1;
	}

	// Comprimir si el modo es 0
	if (atoi(argv[1]) == 0) {
		comprimir(argv[2], argv[3]);
	}
	// Descomprimir si el modo es 1
	else if (atoi(argv[1]) == 1) {
		descomprimir(argv[2], argv[3]);
	}

	return 0;
}
