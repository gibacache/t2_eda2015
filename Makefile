CC=gcc
CFLAGS=-Wall -g
LDLIBS= -lm

all: tarea2

tarea2: skiptree.o qdbmp.o huffman.o list.o

clean:
	rm -f tarea2 skiptree.o qdbmp.o huffman.o list.o

